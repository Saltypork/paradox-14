﻿


EVENT   1
 PAGE   1
  0()



EVENT   2
 PAGE   1
  0()



EVENT   3
 PAGE   1
  0()



EVENT   4
 PAGE   1
  0()



EVENT   5
 PAGE   1
  0()



EVENT   6
 PAGE   1
  0()



EVENT   7
 PAGE   1
  0()



EVENT   8
 PAGE   1
  0()



EVENT   9
 PAGE   1
  0()



EVENT   10
 PAGE   1
  0()



EVENT   11
 PAGE   1
  0()



EVENT   12
 PAGE   1
  0()



EVENT   14
 PAGE   1
  0()



EVENT   15
 PAGE   1
  0()



EVENT   16
 PAGE   1
  0()



EVENT   17
 PAGE   1
  0()



EVENT   18
 PAGE   1
  0()



EVENT   19
 PAGE   1
  0()



EVENT   20
 PAGE   1
  0()



EVENT   13
 PAGE   1
  0()



EVENT   21
 PAGE   1
  // condition: switch 641 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0b,0x73,0x74,0x61,0x67,0x65,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("page257_fc1",0,0,2,1)
  ShowMessage("\\n<Yoko>さあ、本を読みましょう。 読書は人生を豊かにしますよ……")
  ShowMessageFace("page17_fc1",0,0,2,2)
  ShowMessage("\\n<Annie>少年～青年期の読書量と、生涯年収には相関関係がある…… そのような統計データも存在するのだ……")
  ShowMessageFace("page65537_fc1",1,0,2,3)
  ShowMessage("\\n<Cornelia>さあ、最寄りの図書館に行きましょう。 書物はあなたを待っていますよ……")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("\\n<Visitor A>本か、帰りにちょっと買ってみようかな……")
  ShowMessageFace("",0,0,2,5)
  ShowMessage("\\n<Visitor B>子供の頃の絵本以来、読んでなかったなぁ……")
  ShowMessageFace("",0,0,2,6)
  ShowMessage("ステージはそれなりに盛況だ！")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,7)
  ShowMessage("\\n<Manager>なかなか好評だったね。 図書ギルドからも、祝電が届いてるよ。")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1354,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   23
 PAGE   1
  // condition: switch 642 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x69,0x72,0x69,0x61,0x73,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("iriasu_fc4",0,0,2,1)
  ShowMessage("\\n<Ilias>私が創世の女神、イリアスです。 さあ愚かなる民よ、私に祈りを捧げなさい……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<Visitor>おお、イリアス様のコスプレだ！")
  ShowMessageFace("",0,0,2,3)
  ShowMessage("\\n<Female Customer>かわいい～！　ちっちゃな天使様みたい！")
  205(35,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x08,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x13,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x07,0x69,0x00,0x69,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x13,0x3b,0x0c,0x5b,0x07,0x69,0x00,0x69,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
  ShowMessageFace("iriasu_fc4",7,0,2,4)
  ShowMessage("\\n<Ilias>何を言っているのです！ 私こそ、創世の女神イリアスなのですよ！")
  ShowMessageFace("",0,0,2,5)
  ShowMessage("\\n<Female Customer>はねてる！　かわいい～！")
  ShowMessageFace("",0,0,2,6)
  ShowMessage("ステージはそこそこ盛況だ！")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,7)
  ShowMessage("\\n<Manager>なかなか好評でしたね。 でも、教会に怒られないかなぁ……")
  ShowMessageFace("iriasu_fc4",2,0,2,8)
  ShowMessage("\\n<Ilias>私がイリアスです……")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1541,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   24
 PAGE   1
  // condition: switch 643 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x7a,0x75,0x6b,0x61,0x6e,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("stein_fc2",0,0,2,1)
  ShowMessage("\\n<Promestein>それでは、生物学の講義を始めましょう。")
  ShowMessageFace("stein_fc2",0,0,2,2)
  ShowMessage("\\n<Promestein>俗に聞かれる言質ですが…… 魔物を野放しにすると、人間が滅びるというのがあります。")
  ShowMessageFace("stein_fc2",0,0,2,3)
  ShowMessage("\\n<Promestein>基本的に魔物は女性型しか産まないので、増える一方。 このままでは人間は激減し、滅びの道を辿る……")
  ShowMessageFace("stein_fc2",0,0,2,4)
  ShowMessage("\\n<Promestein>しかしこれは、繁殖生態学を完全に無視した考え方です。 どこかの性悪が、無知な民衆に教え込んだのでしょう。")
  ShowMessageFace("stein_fc2",0,0,2,5)
  ShowMessage("\\n<Promestein>原則的に、有性生殖における繁殖は、 生殖スパンの長いメスの方がキーとなります。")
  ShowMessageFace("stein_fc2",0,0,2,6)
  ShowMessage("\\n<Promestein>人間のメスは、生殖に一年弱の時間がかかりますが…… オスの方は、事実上無制限に生殖を行えるのです。")
  ShowMessageFace("stein_fc2",0,0,2,7)
  ShowMessage("\\n<Promestein>そうなりますと、次世代の個体を確保する事において、 オスの重要性はそう大したものではなくなってくるのです。")
  ShowMessageFace("stein_fc2",0,0,2,8)
  ShowMessage("\\n<Promestein>オスは少数でも、多数のメスを孕ませる事ができるので…… 相対的に、オスの価値が落ちる事は分かりますよね？")
  ShowMessageFace("stein_fc2",0,0,2,9)
  ShowMessage("\\n<Promestein>ゆえに男が魔物に対しても種を振りまいたところで…… 人間の次世代個体数は、ほぼ影響しないのです。")
  ShowMessageFace("stein_fc2",0,0,2,10)
  ShowMessage("\\n<Promestein>もし魔物の性質が逆だったなら、人間は絶滅の危機でした。 仮に魔物はオスしか存在せず、オスしか産まれないならば――")
  ShowMessageFace("stein_fc2",0,0,2,11)
  ShowMessage("\\n<Promestein>そうなれば、魔物は人間のメスの子宮を占有し続けます。 そして人間は個体数を増やせず、絶滅の道を辿るでしょう。")
  ShowMessageFace("stein_fc2",0,0,2,12)
  ShowMessage("\\n<Promestein>……もちろん、魔物も後を追うことになりますけど。 結局、生殖を人間に委ねている事に変わりありませんから。")
  ShowMessageFace("stein_fc2",0,0,2,13)
  ShowMessage("\\n<Promestein>……まあ、そういうわけですね。 魔物と交わると人間が滅びるなど、戯言に過ぎません。")
  ShowMessageFace("stein_fc2",0,0,2,14)
  ShowMessage("\\n<Promestein>正しい知識がないと、非科学的な俗説に惑わされます。 学ぶという事がいかに重要か、分かってもらえたでしょうか？")
  ShowMessageFace("",0,0,2,15)
  ShowMessage("\\n<Visitor>……………………")
  ShowMessageFace("",0,0,2,16)
  ShowMessage("ステージは微妙な雰囲気だ……")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace(binary"",0,0,2,17)
  ShowMessage("\\n<Manager>まあ、うん……ここは大学じゃないからねぇ。 次はもっとやわらかい話を頼むよ。")
  ShowMessageFace("stein_fc2",2,0,2,18)
  ShowMessage("\\n<Promestein>がんばったのに……")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1131,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   25
 PAGE   1
  // condition: switch 644 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x18,0x64,0x75,0x6e,0x67,0x65,0x6f,0x6e,0x5f,0x6f,0x62,0x61,0x6b,0x65,0x79,0x61,0x73,0x69,0x6b,0x69,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("chrom_fc2",0,0,2,1)
  ShowMessage("\\n<Chrome>くくく…… それでは、クロムちゃんのネクロマンサー講義を始めよう。")
  ShowMessageFace("frederika_fc1",0,0,2,2)
  ShowMessage("\\n<Frederica>クロム……ちゃんと風呂入ってきたか？")
  ShowMessageFace("chrom_fc2",5,0,2,3)
  ShowMessage("\\n<Chrome>なぜ、それを今言う！ 関係ないじゃろうが……")
  ShowMessageFace("frederika_fc1",0,0,2,4)
  ShowMessage("\\n<Frederica>クロム、珍しく風呂に入る時でも…… 湯船に浸かるだけで、体を洗わない……")
  ShowMessageFace("chrom_fc2",0,0,2,5)
  ShowMessage("\\n<Chrome>それを不潔と思うのは、素人の発想じゃ…… 仮に、石鹸でしっかり洗った場合の綺麗度を10としよう。")
  ShowMessageFace("chrom_fc2",0,0,2,6)
  ShowMessage("\\n<Chrome>では、湯船に浸かるだけで洗わない場合…… 体を洗った10と比べ、浸かるだけの綺麗度は何だと思う？")
  ShowMessageFace("chrom_fc2",1,0,2,7)
  ShowMessage("\\n<Chrome>そこ！　そこの若造、答えてみよ！")
  ShowMessageFace("",0,0,2,8)
  ShowMessage("\\n<Youth>えっと……浸かるだけで洗わないんだろ？ だったら3くらいなんじゃないか？")
  ShowMessageFace("chrom_fc2",1,0,2,9)
  ShowMessage("\\n<Chrome>ハズレじゃな……正解は、8じゃ！ お湯に浸かるだけでも、そこまで綺麗になるのじゃ！")
  ShowMessageFace("chrom_fc2",1,0,2,10)
  ShowMessage("\\n<Chrome>湯に浸かると毛穴が収縮し、表皮の汚れが排出される…… よって、湯船に浸かるだけでも十分に綺麗になるのじゃ！")
  ShowMessageFace("chrom_fc2",1,0,2,11)
  ShowMessage("\\n<Chrome>それに、過度に石鹸で洗いすぎるとかえって肌を傷付ける！ お肌にも優しくないのじゃぞ！")
  ShowMessageFace("",0,0,2,12)
  ShowMessage("\\n<Visitor>（何の講義なんだ……）")
  ShowMessageFace("",0,0,2,13)
  ShowMessage("ステージは微妙な雰囲気だ……")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,14)
  ShowMessage("\\n<Manager>風呂はちゃんと入った方がいいよ……")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1231,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   26
 PAGE   1
  // condition: switch 645 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x6d,0x75,0x72,0x61,0x33,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("lily_fc1",3,0,2,1)
  ShowMessage("\\n<Lily>虐げられた者達…… このリリィ・メーストルが力を与えましょう。")
  ShowMessageFace("wormv_fc1",0,0,2,2)
  ShowMessage("\\n<Julia>この力は、まさに希望…… あらゆる暴虐に屈さず、自由を求める光なのです。")
  ShowMessageFace("suckvore_fc1",2,0,2,3)
  ShowMessage("\\n<Anna>……………………")
  ShowMessageFace("lily_fc1",4,0,2,4)
  ShowMessage("\\n<Lily>さあ、弱き民よ……リリィの元に来なさい。 この私が、皆を導きましょう。")
  ShowMessageFace("",0,0,2,5)
  ShowMessage("\\n<Visitor>……………………")
  ShowMessageFace("",0,0,2,6)
  ShowMessage("ステージは微妙な雰囲気だ……")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,7)
  ShowMessage("\\n<Manager>新興宗教かい？ うちの劇場で、ああいうのはちょっと困るかな……")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1318,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   27
 PAGE   1
  // condition: switch 646 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x6d,0x75,0x72,0x61,0x33,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("lusia_fc1",0,0,2,1)
  ShowMessage("\\n<Lucia>錬金術とは、真理の探究…… 万物の理を追求する、崇高な学問です。")
  ShowMessageFace("c_homunculus_fc1",1,0,2,2)
  ShowMessage("\\n<Crowley>金を錬成するというのは、あくまで卑近な目的の一つ…… 錬金の志とは、すなわち真理に迫る事なのだ……")
  ShowMessageFace("ironmaiden_k_fc1",0,0,2,3)
  ShowMessage("\\n<Torture>知的怠惰に己を浸す者よ、その罰を受けよ…… さもなければ、錬金の道を志せ……")
  ShowMessageFace("lusia_fc1",0,0,2,4)
  ShowMessage("\\n<Lucia>錬金の扉は、誰にでも開け放たれています。 さあ、私と共に真理を追究しようではありませんか！")
  ShowMessageFace("",0,0,2,5)
  ShowMessage("\\n<Visitor>……………………")
  ShowMessageFace("",0,0,2,6)
  ShowMessage("ステージは微妙な雰囲気だ……")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,7)
  ShowMessage("\\n<Manager>新興宗教かい？ うちの劇場で、ああいうのはちょっと困るかな……")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1318,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   28
 PAGE   1
  // condition: switch 647 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0c,0x63,0x61,0x73,0x74,0x6c,0x65,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("sara_fc1",1,0,2,1)
  ShowMessage("\\n<Sara>舞台はよく見に来るけど、自分で立つのは初めてね……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<Visitor A>あの女性、どこかで見覚えが…… まさか、サバサのサラ女王！？")
  ShowMessageFace("",0,0,2,3)
  ShowMessage("\\n<Visitor B>そっくりさんじゃないのか……？ でも、そこはかとなく高貴な雰囲気が……")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0e,0x44,0x61,0x72,0x6b,0x6e,0x65,0x73,0x73,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x18,0x64,0x75,0x6e,0x67,0x65,0x6f,0x6e,0x5f,0x6f,0x62,0x61,0x6b,0x65,0x79,0x61,0x73,0x69,0x6b,0x69,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("sara_fc1",1,0,2,4)
  ShowMessage("\\n<Sara>じゃあ、小さい頃からたしなんでたバイオリンを…… ……やぁっ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0e,0x44,0x61,0x72,0x6b,0x6e,0x65,0x73,0x73,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ShowMessageFace("",0,0,2,5)
  ShowMessage("\\n<Visitor A>なんだこれ、音波攻撃……うぁぁっ！！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0e,0x44,0x61,0x72,0x6b,0x6e,0x65,0x73,0x73,0x33,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ShowMessageFace("",0,0,2,6)
  ShowMessage("\\n<Visitor B>ぎゃぁぁ！　観客に死人が出るぞ！")
  ShowMessageFace("",0,0,2,7)
  ShowMessage("ステージは大混乱だ！")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,8)
  ShowMessage("\\n<Manager>み、耳が……ああ、目眩もする……")
  ShowMessageFace("sara_fc1",4,0,2,9)
  ShowMessage("\\n<Sara>……………………")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1288,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   29
 PAGE   1
  // condition: switch 648 is ON
  241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x16,0x64,0x75,0x6e,0x67,0x65,0x6f,0x6e,0x5f,0x63,0x6f,0x6c,0x6f,0x73,0x73,0x65,0x75,0x6d,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
  ShowMessageFace("saniria_fc2",0,0,2,1)
  ShowMessage("\\n<King of San Ilia>来たれ、機械の国！ 鉄血の体にこそ、真理あり！")
  ShowMessageFace("jeid_fc1",0,0,2,2)
  ShowMessage("\\n<Jaide>ジーク・マキナ！")
  ShowMessageFace("XX-7_fc1",0,0,2,3)
  ShowMessage("\\n<Robin>ジーク・マキナ！")
  ShowMessageFace("saniria_fc2",0,0,2,4)
  ShowMessage("\\n<King of San Ilia>機械こそ不滅！　機械こそ安寧！")
  ShowMessageFace("kaniloid_fc1",0,0,2,5)
  ShowMessage("\\n<Jillian>ジーク・マキナ！")
  ShowMessageFace("brunhild_fc1",0,0,2,6)
  ShowMessage("\\n<Hild>ジーク・マキナ！")
  ShowMessageFace("saniria_fc2",0,0,2,7)
  ShowMessage("\\n<King of San Ilia>自由意志などまやかしに過ぎん！ この世はしょせん、機械なり！")
  ShowMessageFace("valt_fc1",0,0,2,8)
  ShowMessage("\\n<Valto>ジーク・マキナ！")
  ShowMessageFace("radio_fc1",0,0,2,9)
  ShowMessage("\\n<Radio>ジーク・マきナ！")
  ShowMessageFace("saniria_fc2",0,0,2,10)
  ShowMessage("\\n<King of San Ilia>さあ、皆の者！ 機械の国で、ネジとなるのだ！")
  ShowMessageFace("",0,0,2,11)
  ShowMessage("\\n<Visitor>……………………")
  ShowMessageFace("",0,0,2,12)
  ShowMessage("ステージは静まり返っている……")
  Wait(30)
  221()
  242(1)
  Wait(60)
  211(1)
  281(1)
  TeleportPlayer(0,624,63,27,6,2)
  ChangeSwitch(2119,2119,0)
  222()
  ShowMessageFace("",0,0,2,13)
  ShowMessage("\\n<Manager>いやぁ、緊迫感のある舞台でした。 もちろん、冗談ですよね……")
  ShowMessageFace("saniria_fc2",0,0,2,14)
  ShowMessage("\\n<King of San Ilia>当然、ただの冗談だよ……")
  If(2,"A",1)
   ChangeSelfSwitch("A",0)
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventoryArmor(1363,0,0,1,false)
   0()
  EndIf()
  ChangeSwitch(2119,2119,1)
  EndEventProcessing()
  0()



EVENT   22
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   31
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   48
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   46
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   49
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   47
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   50
 PAGE   1
  // condition: switch 648 is ON
  0()



EVENT   34
 PAGE   1
  // condition: switch 641 is ON
  0()
 PAGE   2
  // condition: switch 646 is ON
  0()
 PAGE   3
  // condition: switch 645 is ON
  0()



EVENT   33
 PAGE   1
  // condition: switch 641 is ON
  0()
 PAGE   2
  // condition: switch 645 is ON
  0()
 PAGE   3
  // condition: switch 646 is ON
  0()



EVENT   32
 PAGE   1
  // condition: switch 641 is ON
  0()
 PAGE   2
  // condition: switch 645 is ON
  0()
 PAGE   3
  // condition: switch 646 is ON
  0()



EVENT   35
 PAGE   1
  // condition: switch 642 is ON
  0()
 PAGE   2
  // condition: switch 647 is ON
  0()
 PAGE   3
  // condition: switch 643 is ON
  0()



EVENT   36
 PAGE   1
  // condition: switch 644 is ON
  0()



EVENT   37
 PAGE   1
  // condition: switch 644 is ON
  0()
